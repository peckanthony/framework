"""empty message

Revision ID: 63cc7e5c6f71
Revises: None
Create Date: 2016-09-02 18:02:38.161780

"""

# revision identifiers, used by Alembic.
revision = '63cc7e5c6f71'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('contacts',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('firstName', sa.String(length=100), nullable=True),
    sa.Column('lastName', sa.String(length=100), nullable=True),
    sa.Column('dateOfBirth', sa.DateTime(), nullable=True),
    sa.Column('address', sa.String(length=200), nullable=True),
    sa.Column('postalCode', sa.String(length=10), nullable=True),
    sa.Column('city', sa.String(length=100), nullable=True),
    sa.Column('country', sa.String(length=100), nullable=True),
    sa.Column('jobDescription', sa.String(length=200), nullable=True),
    sa.Column('company', sa.String(length=200), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('contacts')
    ### end Alembic commands ###
