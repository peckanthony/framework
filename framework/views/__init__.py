from flask import Blueprint

setup = Blueprint('setup', __name__, template_folder='templates/setup')

from framework.views import setup
