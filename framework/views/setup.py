from . import setup
from flask import render_template, redirect, url_for
from framework.forms import company
from framework.controllers import company

controller = Controller()

@setup.route('/setup/company')
def company():
    projects = controller.getProjects()
    return render_template('project/list.html', header='Projects', projects=projects)

@project.route('/project/<projectId>/edit', methods=['GET', 'POST'])
def editProject(projectId):
    generalForm = ProjectFormGeneral()
    taskForm = ProjectFormTask()
    budgetForm = ProjectFormBudget()

    #Validations
    if generalForm.validate_on_submit():
        controller.validateGeneralForm(projectId, generalForm)
        return redirect(url_for('.project', projectId=projectId))

    if taskForm.validate_on_submit():
        controller.validateTaskForm(projectId, taskForm)
        return redirect(url_for('.project', projectId=projectId))

    if budgetForm.validate_on_submit():
        controller.validateBudgetForm(projectId, budgetForm)
        return redirect(url_for('.project', projectId=projectId))

    #Edit project
    if projectId is not None:
        project = controller.getProjectOr404(projectId)

        generalForm.name.data = project.name
        generalForm.description.data = project.description
        taskForm.taskInfo.data = project.taskInfo
        budgetForm.budgetInfo.data = project.budgetInfo

    return render_template('project/editcard.html', project=project, generalForm=generalForm, taskForm=taskForm, budgetForm=budgetForm)

@project.route('/project/new', methods=['GET', 'POST'])
def newProject():
    generalForm = ProjectFormGeneral()

    del generalForm.status

    if generalForm.validate_on_submit():
        projectId = controller.createProject(generalForm)
        return redirect(url_for('.project', projectId=projectId))

    return render_template('project/new.html', projectForm=generalForm)

@project.route('/project/<projectId>', methods=['GET'])
def project(projectId):
    project = controller.getProjectOr404(projectId)
    return render_template('project/card.html', project=project)