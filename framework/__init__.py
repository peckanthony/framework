from flask import Flask
from flask.ext.bootstrap import Bootstrap
from flask.ext.moment import Moment
from flask.ext.sqlalchemy import SQLAlchemy
from config import config

bootstrap = Bootstrap()
moment = Moment()
db = SQLAlchemy()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    moment.init_app(app)
    db.init_app(app)

    configure_blueprints(app)

    return app

def configure_blueprints(app):
    """Configure blueprints in views."""

    from framework.contacts import contacts

    for bp in [contacts]:
        app.register_blueprint(bp)

    return app
