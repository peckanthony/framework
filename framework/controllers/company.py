from framework.models import company
from framework.forms import company
from .. import db

class CompanyController:

    def getProject(self, projectId):
        return Project.query.filter_by(id=projectId).first()

    def getStatusByName(self, status):
        return ProjectStatus.query.filter_by(status=status)

    def getProjectOr404(self, projectId):
        return Project.query.filter_by(id=projectId).first_or_404()

    def getProjects(self):
        return Project.query.all()

    def createProject(self, projectFrom):
        project = Project(name=projectFrom.name.data,
                          description=projectFrom.description.data,
                          projectStatus=ProjectStatus.query.filter(
                              ProjectStatus.status=='Created'
                          ).first(),
                          owner='Anthony'
                          )

        db.session.add(project)
        db.session.flush()

        return project.id

    def validateGeneralForm(self, projectId, generalForm):
        project = self.getProject(projectId)

        project.name = generalForm.name.data
        project.description = generalForm.description.data

        db.session.commit()

    def validateTaskForm(self, projectId, taskForm):
        project = self.getProject(projectId)

        project.taskInfo = taskForm.taskInfo.data

        db.session.commit()

    def validateBudgetForm(self, projectId, budgetForm):
        project = self.getProject(projectId)

        project.budgetInfo = budgetForm.budgetInfo.data

        db.session.commit()