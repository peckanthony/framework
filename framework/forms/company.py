from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, SubmitField, SelectField
from wtforms.validators import DataRequired, Optional, ValidationError
from pyvat import check_vat_number

class ContactForm(Form):
    name = StringField('Company name', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    vatNumber = TextAreaField('VAT No', validators=[DataRequired(), vatValidation])
    #status = SelectField('Status', validators=[DataRequired()])
    submit = SubmitField('Save')

def vatValidation(form, field):
    if not( check_vat_number(field) ):
        raise ValidationError('VAT number is not valid')
