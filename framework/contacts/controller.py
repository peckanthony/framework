from .. import db
from framework.contacts.models import Contact

class ContactController:

    def getContacts(self):
        return Contact.query.all()
