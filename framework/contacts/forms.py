from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, IntegerField, SubmitField, SelectField, DateField, widgets
from wtforms.validators import DataRequired, Optional, ValidationError

class ContactForm(Form):
    firstName = StringField('First name')
    lastName = StringField('Last name')
    company = StringField('Company')
    jobDescription = StringField('Job description')

    address = StringField('Address')
    postalCode = StringField('Postal code')
    city = StringField('City')
    country = StringField('Country')

    dateOfBirth = DateField('Date of birth', format = '%d/%m/%Y', validators=(Optional(),))

    #status = SelectField('Status', validators=[DataRequired()])

    submit = SubmitField('Save')

    def validate(self):
        if not Form.validate(self):
            return False
        result = False
        for field in [self.firstName, self.lastName, self.company]:
            if field.data != '':
                result = True
        if result == False:
            for field in [self.firstName, self.lastName, self.company]:
                field.errors.append('Please enter a name or company')
        return result