from . import contacts
from .. import db
from framework.contacts.controller import ContactController
from framework.contacts.forms import ContactForm
from flask import render_template, redirect, url_for, request

from flask_wtf import Form
from wtforms.ext.sqlalchemy.orm import model_form
from framework.contacts.models import Contact

contactController = ContactController()
dict = {'Title': 'Apeck Webdev', 'Header': 'Contacts', 'Age': 7}

@contacts.route('/contacts/')
def company():
    contacts = contactController.getContacts()
    return render_template('list.html', title=dict['Title'], header=dict['Header'], items=contacts)

@contacts.route('/contacts/new/', methods=['GET', 'POST'])
@contacts.route('/contacts/edit/<id>/', methods=['GET', 'POST'])
def editProject(id=None):
    if id is not None:
        model = Contact.query.filter_by(id=id).first()
        form = ContactForm(request.form, model)
    else:
        model = Contact()
        form = ContactForm()

    if form.validate_on_submit():
        form.populate_obj(model)

        db.session.add(model)
        db.session.flush()

        #flash("Contacts updated")
        return redirect(url_for("contacts.editProject", id=model.id))
    return render_template("contacts/editcard.html", form=form, id=id)