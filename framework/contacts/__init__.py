from flask import Blueprint

contacts = Blueprint('contacts', __name__, template_folder='contacts/pages')

from . import views
