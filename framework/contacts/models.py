from framework import db
import datetime

class Contact(db.Model):
    __tablename__ = 'contacts'
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(100))
    lastName = db.Column(db.String(100))
    dateOfBirth = db.Column(db.DateTime)

    address = db.Column(db.String(200))
    postalCode = db.Column(db.String(10))
    city = db.Column(db.String(100))
    country = db.Column(db.String(100))

    jobDescription = db.Column(db.String(200))
    company = db.Column(db.String(200))

    def title(self):
        title = self.firstName
        if title == '':
            title = self.lastName
        else:
            title += ' ' + self.lastName

        if title == '':
            title = self.company

        return title

    def description(self):
        description = ''
        for str in [self.jobDescription, self.company]:
            if description != '' and str != '':
                description += ' @ '
            if str != '':
                description += str

        return description

    """
    def address(self, isHtmlString):
        address = ''
        for str in [self.addressLine1, self.addressLine2]:
            if address != '' and str != '':
                address += ' '
            if str != '':
                address += str

        if address != '':
            address += ', '
            if isHtmlString:
                address += '<br />'
            else:
                address += '\n'

        for str in [self.postalCode, self.city, self.country]:
            if address != '' and str != '':
                address += ' '
            if str != '':
                address += str
    """